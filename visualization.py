#!/usr/bin/env python
# coding: utf-8
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import Preprocessing

def visu3D(datadict,composante1,composante2,composante3):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(datadict.get("data")[:,composante1], datadict.get("data")[:,composante2], datadict.get("data")[:,composante3], c=datadict.get("labels"), marker='o')
    
    ax.set_xlabel("X Label")
    ax.set_ylabel("Y Label")
    ax.set_zlabel("Z Label")
    plt.show()

def visu2D(datadict,composante1,composante2):
    plt.scatter(datadict.get("data")[:,composante1], datadict.get("data")[:,composante2], c=datadict.get("labels"), marker='o')
    plt.show()

if __name__ == '__main__':
    kidney, kidney_pca, kidney_tsne, banknote, banknote_pca, banknote_tsne = Preprocessing.preprocess_main(3,3)
    
    print(banknote.get("data").shape)
    print(banknote.get("labels").shape)
    
    print(banknote_tsne.get("data").shape)
    print(banknote_tsne.get("labels").shape)
    #visu2D(banknote_pca,0,1)
    visu3D(kidney_pca,0,1,2)
    
    