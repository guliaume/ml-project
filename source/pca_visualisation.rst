Visualisation of PCA results
=================================

.. automodule:: pca_visualisation
   :members:
   :undoc-members:
   :show-inheritance:
