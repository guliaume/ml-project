
"""
Author: Imane M\n
Plots of ROC curves for SVM model with the best parameters for each dataset
"""

import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.svm import SVC
from Preprocessing import preprocess_main

#data
kidney_pca = preprocess_main()[2]
banknote = preprocess_main()[0]

# generate a no skill prediction (majority class)
ns_probs = [0 for _ in range(len(banknote["data_test"]))]

svc = SVC(C=1, kernel='rbf', gamma=1, probability=True)
svc.fit(banknote["data_train"],banknote["label_train"])

# predict probabilities
#svc_probs = svc.predict_proba(kidney_pca["data_test"])
svc_probs = svc.predict_proba(banknote["data_test"])

# keep probabilities for the positive outcome only
svc_probs = svc_probs[:, 1]
#svc_probs = svc_probs[:, 1]

# calculate scores
ns_auc = roc_auc_score(banknote["label_test"], ns_probs)
svc_auc = roc_auc_score(banknote["label_test"], svc_probs)
#ns_auc = roc_auc_score(kidney_pca["label_test"], ns_probs)
#svc_auc = roc_auc_score(kidney_pca["label_test"], svc_probs)

# summarize scores
print('No Skill: ROC AUC=%.3f' % (ns_auc))
print('SVC: ROC AUC=%.3f' % (svc_auc))

# calculate roc curves
ns_fpr, ns_tpr, _ = roc_curve(banknote["label_test"], ns_probs)
svc_fpr, svc_tpr, _ = roc_curve(banknote["label_test"], svc_probs)
#ns_fpr, ns_tpr, _ = roc_curve(kidney_pca["label_test"], ns_probs)
#svc_fpr, svc_tpr, _ = roc_curve(kidney_pca["label_test"], svc_probs)

# plot the roc curve for the model
plt.plot(ns_fpr, ns_tpr, linestyle='--', label='No Skill')
plt.plot(svc_fpr, svc_tpr, marker='.', label='SVM')

# axis labels
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
# show the legend
plt.legend()
plt.title("SVM ROC curve on Banknote dataset")
#plt.title("SVM ROC curve on Kidney dataset (reduced with PCA)")

# show the plot
plt.show()
